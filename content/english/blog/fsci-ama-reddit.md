---
title: "FSCI will do an Ask Me Anything on r/India subreddit on 12th April"
date: 2022-04-08T17:02:12+05:30
author: "false"
tags: ["reddit", "AMA"]
draft: false
discussionlink: "https://codema.in/d/GVqe9oHL/fsci-s-ask-me-anything-on-reddit"
---
*Update: The AMA ended and you can read it [here](https://libredd.it/r/india/comments/u1wolh/we_are_the_free_software_community_of_india_fsci/).*

We are invited by the moderators of [r/India subreddit](https://reddit.com/r/india) to do an [Ask Me Anything](https://mods.reddithelp.com/hc/en-us/articles/360002086132-What-is-an-AMA-and-why-would-I-host-one-) as a community. Members of r/India community can ask us anything during this Ask Me Anything session. We feel that this is a very good opportunity to interact with people and spread the idea of [Free Software](https://www.gnu.org/philosophy/free-software-even-more-important.html) and its importance.

The AMA is on 12th April 2022 and will start from 17:00 hours [Indian Standard Time](https://en.wikipedia.org/wiki/Indian_Standard_Time). We created a Reddit account especially for this AMA and our username on Reddit is [u/fsci-in](https://reddit.com/user/fsci-in/).

Thanks a lot to the r/India moderators for the invitation. Feel free to join the AMA and literally ask us anything. We are looking forward to the AMA and hope it will be fun.

Links:

- [AMA announcement on r/India](https://libreddit.kavin.rocks/r/india/comments/tz1qo5/ama_announcement_the_team_behind_free_software/).

- [Announcement of Mastodon](https://mastodon.online/@redditindia/108096357296625884) by r/India.

- [Announcement on Twitter](https://bird.trom.tf/redditindia/status/1512400728475815945) by r/India.

