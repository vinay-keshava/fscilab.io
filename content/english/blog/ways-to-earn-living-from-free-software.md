---
title: "How do people make a living out of free software"
date: 2022-02-07
draft: false
author: "false"
DiscussionLink: "https://codema.in/d/N2ywvJCk/an-article-on-how-free-software-projects-are-funded"
---
(This article is taken from a draft created for [FSF India](https://fsf.org.in) article on the same topic)

When people understand that "[free software](https://ravidwivedi.in/free-software)" means the software which respects user's freedom and not free of cost, the question "how does one earn a living with free software?" comes from the fact that free software ensures the user's freedom to share exact and modified copies of the software with anyone.

To answer this question arising out of natural curiosity, we have compiled a list of methods on how to make a living out of free software or to sustain the development of free software with examples.

### 1. Free Software consultancy services which provide solutions based on free software

A business can provide paid consultation services to clients and implement solutions based on free software. [DeepRoot GNU/Linux](https://deeproot.in) is an example of a successful Free Software consultancy service.

Quote from Abhas who leads Deeproot: "At DeepRoot Linux, we enable organisations of all sizes to use Free Software for their infrastructure deployments. We provide software, services and support so that they can use GNU/Linux and Free Software effectively. Over the past 18 years, we have worked with more than 500 organisations to help them in their endeavour to adopt and run Free Software. We build mail servers and mail server clusters, private clouds, containerisation and automation solutions, computer labs and so much more. We provide support for a variety of free software tools that user’s might want to use and help them implement, scale and secure them."


### 2. Customization of Free Software

Free Software businesses can customize their own offering or existing free software for their clients to satisfy their needs. This is usually done in combination with selling free software solutions.


For example, [Alphafork Technologies](https://alphafork.com) is a business which [customizes Free Software to build solutions according to the requirements of clients](https://poddery.com/posts/4691002).


[Computing Freedom Collective](https://cfc.net.in) is another business which provides customized solutions based on Free Software.


### 3. Offering Free Software powered services 

Free Software services can be setup and run with an access fee for using the services.

Examples:

- [Chiguru Class Meet](https://classmeet.chiguru.tech/) offers gratis and paid plans for using their BigBlueButton service, which is a Free Software Video Conferencing solution.

- [conversations.im](https://account.conversations.im/) offers a paid XMPP service.

### 4. By selling hardware with free software pre-installed.

Examples: 

- [Unmukti](https://unmukti.in) is a unique free software business selling hardware devices handling network security and connectivity for clients, with customized free software solutions running on top of it.

- [Mostly Harmless](https://mostlyharmless.io/) sells libre hardware, like laptops, phones, routers etc., capable of running only free software.

- [Purism](https://puri.sm/) is a company which sells hardware which can run fully free software. They maintain their operating system, PureOS and the development of PureOS are funded by selling hardware. 

### 5. By selling free software 

[People can charge as much as they wish or can for a copy of free software](https://www.gnu.org/philosophy/selling.html). If a license does not permit users to make copies and sell them, it is a nonfree license. Free/Swatantra software is a matter of user's freedom and the following are some examples of distributing free software for a fee:

Examples:

- [Simple Mobile Tools](https://www.simplemobiletools.com/), an android app developer, sells a huge variety of android apps including a gallery app, calendar, camera, clock, app launcher, calculator, dialer, sms messenger, voice recorder and many more apps in two ways. One is, they sell paid apps via google play store and the same apps are distributed via F-Droid without charging any money. So the user can get the same app for free and can also support its development by paying for it if they so desire.

- Krita Foundation, sells Krita, a free software digital painting application, on Windows store and on Steam for a fee. 

- Ardour is another free software project that shows free is all about [freedom not gratis](http://ardour.org/faq.html#download).

- [Conversations](https://play.google.com/store/apps/details?id=eu.siacs.conversations&referrer=utm_source%3Dwebsite) is a paid free software Android xmpp app which can be downloaded from Google Play Store.

- [Vital](https://vital.audio/#getvital), a spectral warping wavetable synthesizer is a free software sold  for a fee.

- [Elementary OS](https://elementary.io/), which is a GNU/Linux Distribution makes downloads available with options for payment.

- [Zorin OS](https://zorinos.com), which is another GNU/Linux Distribution, funds the developments of its software by selling paid versions of free software.

### 6. Selling Exceptions to the GNU GPL

Instead of making a software available under dual licenses, a free software can be sold to someone making an exception in case of application of free software license terms. It is sold to the buyer only without any possibility of redistributing the software under that license exception. See [Selling Exceptions](https://www.gnu.org/philosophy/selling-exceptions.html) for more details.


### 7. Selling Free/Swatantra core with paid addons: 

Providing addons for a fee for extra functionality in a free (as in freedom) software (the software itself can be paid or gratis). 

- [VCV Rack](https://github.com/VCVRack/Rack/blob/v1/LICENSE.md)

- Akaunting, an accounting software that is free-of-cost for use but also has paid addons for additional functionality https://akaunting.com/

### 8. Subscription based support: 

A Free Software business can sell subscriptions for support which ensures clients using the supported free software can rely on that business for support when they need it. 

Examples:
 
- Red Hat sells subscriptions for the support, training, and integration services that help customers in using their free software products. Customers pay one set price for unlimited access to services such as [Red Hat Network and up to 24/7 support](https://blog.executivebiz.com/2014/06/draft-red-hat-selects-genesys-cloud-contact-center-solution-to-transform-customer-experience/). 

- [Suse Linux](https://www.suse.com/support/) also offers the same.

- [Matrix.org](https://matrix.org) develops free replacements to proprietary/nonfree apps like Whatsapp, Telegram which can be installed on own infrastructure and they [offer paid hosting based subscriptions](https://element.io/pricing) which funds the development of the software.

### 9. Donations and Crowdfunding

- Donations are usually collected for Free Software Development and hosting free software services for the public. 

- [GCompris](https://gcompris.net), [Godot game engine](https://godotengine.org), [Blender](https://blender.org), [Peertube, a decentralized video platform](https://joinpeertube.org/), [Pixelfed, a decentralized photo sharing platform](https://pixelfed.org/) are examples of free softwares developed with community funding which ensures livelihood of developers.


- [Riseup](https://riseup.net), [Disroot](https://disroot.org), etc. collect donations for running various Free Software Services for the community.

### 10. Free Software Training

Charging money for providing trainings for free software based services.

- Blender Studio is funded through [Blender Cloud](https://cloud.blender.org), which offers training and learning materials to learn Blender.

### 11. Selling Merchandise

Selling merchandise branded with free software logos such as clothes, accessories, mugs, pens etc. can be helpful in sustaining free software development and in paying developers.

- Blender sells merchandise at [Blender Store](https://store.blender.org/), KDE sells [laptops, merchandise, books on their MetaStore](https://kde.org/stuff/metastore/), same for [Matrix foundation merchandise store](https://shop.matrix.org/)

### 12. Content creation around Free Sofware

Examples:

- [It's FOSS](https://itsfoss.com) an online technology website has built its entire content around free sofware and their own content can be reused with proper credits.

- Content creators like [Novaspirit Tech](https://yewtu.be/channel/UCrjKdwxaQMSV_NDywgKXVmw) [Chris Were](https://yewtu.be/channel/UCAPR27YUyxmgwm3Wc2WSHLw), [Luke Smith](https://yewtu.be/channel/UC2eYFnH61tmytImy1mTYvhA) and many more have built their audience around free sofware where they build various projects, do experiments, discuss free sofware and because of their reach, have access to sponsorships from brands and donations from users inclduing Patreon.

- [Tilvids.com](https://tilvids.com/), a curated, donation based edutainment video platform built on the Free Peertube software. The donations to the website are used to maintain the website as well as for commissioning content creators to create original content.

### 13. By getting paid to develop free software.

- Software like Libreoffice, OBS studio, KDE softwares are examples of free software which are being developed by corporate sponsorships to developers who are commissioned to fix particular issues or develop new functionality. The corporates get the benefit of using free software with functionality they want and the wider community benefits from improved software.

You can watch [this video](https://videos.fsci.in/videos/watch/cca5981d-8513-4448-8cb6-195f2c9db648) for more details where people earning a living from Free Software shre their experiences. This was a discussion session organized as a part of [Free Software Camp 2020](https://camp.fsf.org.in).

FSF maintains a [directory of people offering their free software services for hire](https://www.fsf.org/resources/service).

A lot of free software developers all over the world are paid for their work, others volunteer to develop free software in their free time. We consider it as developer's obligation to release the software under a [free license](https://www.gnu.org/licenses/license-list.html).

A lot of free software projects are developed by volunteers in which non-technical people are also involved in crowdfunding, designing posters, campaigning by writing articles, translation into local languages etc. Free Software community needs everyone to get involved to create a free society. You can contribute to free software by volunteering for FSCI. You can [contact us](/contact) for guidance on how you can make a living with free software.

## Further Reading: 

- A website maintained by Abhas explaining what is a [Free Software Business](https://freesoftware.business/) - Raises awareness, explores the possibilities of running one.   

- Free software in business: [Success stories](https://www.fsf.org/blogs/community/free-software-in-business-success-stories).

