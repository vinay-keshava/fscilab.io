---
title: "Contributors"
---

<style>
table, td, th {  
  border: 1px solid #ddd;
  text-align: left;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 15px;
}
</style>

List of all the contributors to FSCI's 2022 Funding Campaign (in INR).

<br> <br>
**Recieved amount** - 11,100 INR <br>
**Target amount** - __ INR <br>
**Last updated** - 03 February 2022 

<table>
  <tr>
    <th>Name of the donor</th>
    <th>Amount donated(INR)</th>
    <th>Service for which donation was made</th>
  </tr>
  <tr>
    <td>Ravi Dwivedi</td>
    <td>1000</td>
    <td>XMPP poddery</td>
  </tr>
  <tr>
    <td>Deepak Gothwal</td>
    <td>500</td>
    <td>poddery.com</td>
  </tr>
  <tr>
    <td>Sahil Dhiman</td>
    <td>2000</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Abhilash V</td>
    <td>600</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Nilesh</td>
    <td>1000</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>GN</td>
    <td>5000</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Dhanesh</td>
    <td>500</td>
    <td>FSCI General fund</td>
  </tr>
  <tr>
    <td>Ravi</td>
    <td>500</td>
    <td>matrix poddery</td>
  </tr>
    <td></td>
    <td><b>Total Amount = 11,100</b></td>
    <td></td>
  </tr>
</table>


### We are very grateful to our contributors for supporting the ideals behind running this service.
<br>
<br>
<br>
<br>
<br>
<br>
<br>
