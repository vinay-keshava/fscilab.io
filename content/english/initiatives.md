---
title: "Initiatives"
---

Ongoing Initiatives/Campaigns

The following initiatives are ongoing as part of FSCI.

- [Privacy Yathra](https://privacyyathra.in): Privacy Yathra is a campaign by Free Software Community of India to raise awareness about digital privacy. The campaigners of FSCI aim to travel across India, which is what the word ‘yathra’ means in many Indian languages. The website provides a privacy guide to an average computer or mobile user.

- Outreach Team: Our outreach team is to talk to people outside of the community, such as talking to people who write to us. Since communication with people is very important to build a movement, outreach is a very important part of our activities. Let us know by sending an email to contact at fsci dot in if you would like to get involved in the outreach team.

- Software Freedom Camp: Every year we conduct a camp to introduce people to Free Software, how they can start using Free Software and mentor them to contribute to Free Software projects. We are planning to conduct Software Freedom Camp this year as well. Please help us in conducting the year 2022. If you are interested, please check [this page](https://codema.in/d/2BFJNLuG/organizing-software-freedom-camp-2022-).

- Open Letter to Kerala Teachers: This campaign aims to convince the educational institutes of Kerala to switch to Free Software, and was a response to KITE accepting Google's free-of-cost offer of G-Suite into the classrooms. [Sign the letter](/blog/letter-to-kerala-teachers/) today.

- Promoting Free Software in education: This campaign aims to convince educational institutes to switch to free software and provide technical help. Educational institutes interested in switching to free software, please contact us by writing an email to contact at fsci dot in. Join for discussion:

  - [Mailing list](https://lists.fsci.in/postorius/lists/fs-edu.fsci.in/).

  - Web-based [collaborative decision making group](https://codema.in/free-software-community-of-india-fsci-free-software-for-education/).

  - [XMPP English group](https://join.jabber.network/#fs-edu@groups.poddery.com?join).

  - [XMPP Hindi group](https://join.jabber.network/#fs-edu-hi@groups.poddery.com?join).

  - [Matrix](https://matrix.to/#/#fs-edu:poddery.com).

- Fixing the matrix-XMPP bifrost bridge: Matrix-XMPP bifrost bridge has a bug that [XMPP users miss messages](/blog/xmpp-matrix-bridge/) sent in the group when they were offline when the group is hosted on matrix. We are working on fixing this. Please read more on the [opencollective page of the project](https://opencollective.com/bifrost-mam). Please donate to the project if you would like to help fixing this.

### How to Get Involved

Visit our [contact page](/contact) if you want to get involved in any of these campaigns. Or you can check other options to [join us](#join-us). You can also write an email to contact at fsci dot in to get involved in these campaigns.
